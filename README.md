# Idealize.io

Idealize is a platform to discover open solutions and solve each others problems. We believe there’s a better way to bring these solutions to market, that’s transparently and together.

